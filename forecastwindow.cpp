#include "forecastwindow.h"
#include "windowmanager.h"
#include "ui_forecastwindow.h"


ForecastWindow::ForecastWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ForecastWindow)
{
    ui->setupUi(this);
    this->ui->pushButtonForecast->setEnabled(false);
}

ForecastWindow::~ForecastWindow()
{
    delete ui;
}


void ForecastWindow::on_pushButtonClose_clicked()
{
    this->close();
}

void ForecastWindow::on_pushButtonCurrentState_clicked()
{
   WindowManager::Change_active_window("CurrentState");
}
