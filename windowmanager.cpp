#include "windowmanager.h"

std::string WindowManager::active_window = "";
std::map<std::string, QWidget*> WindowManager::window_map = std::map<std::string, QWidget*>();

WindowManager::WindowManager(QWidget* starting_window, std::string name)
{
    if(starting_window == nullptr)
        return;
    WindowManager::active_window = name;
    WindowManager::window_map[name] = starting_window;
}

void WindowManager::add_window(std::string name, QWidget* window)
{
    WindowManager::window_map[name] = window;
}

void WindowManager::remove_window(std::string name)
{
    auto it = WindowManager::window_map.find(name);
    WindowManager::window_map.erase(it);
}

void WindowManager::Change_active_window(std::string name)
{
    if(WindowManager::active_window != "")
    {
        auto it1 = WindowManager::window_map.find(WindowManager::active_window);
        QWidget* old_window = it1 -> second;
        old_window->hide();
    }
    auto it2 = WindowManager::window_map.find(name);
    QWidget* new_window = it2 -> second;
    new_window->activateWindow();
    new_window->raise();
    new_window->show();
    WindowManager::active_window = it2 -> first;
}
