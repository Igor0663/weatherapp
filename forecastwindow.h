#ifndef FORECASTWINDOW_H
#define FORECASTWINDOW_H

#include <QWidget>

namespace Ui {
class ForecastWindow;
}

class ForecastWindow : public QWidget
{
    Q_OBJECT

public:
    explicit ForecastWindow(QWidget *parent = nullptr);
    void  ConnectMainWindow(QWidget * main = nullptr);
    ~ForecastWindow();

private slots:
    void on_pushButtonClose_clicked();

    void on_pushButtonCurrentState_clicked();

private:
    Ui::ForecastWindow *ui;
};

#endif // FORECASTWINDOW_H
