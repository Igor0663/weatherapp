#include "mainwindow.h"
#include "windowmanager.h"
#include "forecastwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    ForecastWindow f;
    MainWindow w(nullptr);
    WindowManager::add_window("CurrentState", &w);
    WindowManager::add_window("Forecast", &f);

    WindowManager::Change_active_window("CurrentState");
    return a.exec();
}
