#include "mainwindow.h"
#include "ui_mainwindow.h"

#include<QDebug>
#include<QString>
#include<QList>
#include<QFile>
#include<QDataStream>
#include<QFloat16>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->ui->pushButtonCurrentState->setEnabled(false);
    QList<QString> cities;

    QString fileName(":/files/files/MiastaFinal.txt");
    QFile file(fileName);

    if(!file.open(QIODevice::ReadOnly))
        qDebug() << "Town description file did not open!\n";

    while(!file.atEnd())
    {
        QString line = file.readLine();
        QString city = line.split(" ").front();
        city.replace("_", " ");
        cities.append(city);
    }
    file.close();
    this->ui->comboBoxLocalization->addItems(cities);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButtonForecast_clicked()
{
    WindowManager::Change_active_window("Forecast");
}
