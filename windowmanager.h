#ifndef WINDOWMANAGER_H
#define WINDOWMANAGER_H
#include<QWidget>
#include<string>
#include<map>

class WindowManager
{
        static std::string active_window;
        static std::map<std::string, QWidget*> window_map;

        WindowManager(QWidget* starting_window = nullptr, std::string name = "");
    public:
        static void Change_active_window(std::string name);
        static void add_window(std::string name, QWidget* window);
        static void remove_window(std::string name);
};

#endif // WINDOWMANAGER_H
